-- Latihan ulang penjelasan yang diberikan di kelas

-- Recursion (Reduction)
listSum :: [Int] -> Int
listSum [] = 0
listSum(a:as) = a + listSum as
-- listSum [1,2,3,4,5]


-- Function Expression
easy :: Int -> Int -> Int -> Int
easy x y z = x*(y+z)
-- easy 4 2 6


-- Optional Function Params
fun1 = easy 4 2
-- fun1 6 -- would be the same as easy 4 2 6


-- Anonymous Function
funLambda = \n -> \a -> \b -> n*(a+b)
-- funLambda 4 2 6 -- would be the same as easy 4 2 6



-- Function that flips function Params
myFlip f x y = f y x
newEasy = myFlip easy
-- newEasy 2 4 6 -- would be the same as easy 4 2 6

newEasyC n = myFlip (easy n)
-- newEasyC 4 6 2 -- would be the same as easy 4 2 6



-- NOTES: /= is != in Haskell.
-- NOTES: !! is indexing in Haskell, [1,2,3] !! 1 would return 2
