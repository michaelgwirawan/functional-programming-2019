-- Practicing after Quiz

-- KPK
kpk :: Int -> Int -> Int
kpk x 0 = 0
-- kpk 0 y = 0
kpk x y = [z | z <- (map (*x) [1..]), z `mod` y == 0] !! 0
-- This would enough to handle (kpk 12 0) and (kpk 0 12)
-- Line 6 is not needed, (map (*x) [1..]) is already 0, thus 0 mod anything is 0

-- FPB
fpb :: Int -> Int -> Int
fpb x y | x < y = fpb y x
fpb x 0 = x
fpb x y = fpb y (x `mod` y)
-- Keep in mind, x should always be bigger than y
-- If x < y, flip the params



-- Merge Sort
merge :: [Int] -> [Int] -> [Int]
merge xxs@(x:xs) yys@(y:ys) | x == y = x:merge xs ys
                            | x < y = x:merge xs yys
                            | x > y = y:merge xxs ys
merge kiri [] = kiri
merge [] kanan = kanan

mergesort :: [Int] -> [Int]
mergesort [] = []
mergesort [a] = [a]
mergesort xs = merge (mergesort kiri) (mergesort kanan)
  where kiri = take ((length xs) `div` 2) xs
        kanan = drop ((length xs) `div` 2) xs

-- Tried wihtout line 32, stuck there for 30 mins approximately
-- But shouldn't it be handled with merge instead?


-- Quick Sort
quicksort :: [Int] -> [Int]
quicksort [] = []
quicksort (x:xs) = quicksort ([z | z <- xs, z <= x]) ++ [x] ++ quicksort ([z | z <- xs, z > x])
-- I got an error while declaring: quicksort x:xs, should be quicksort (x:xs) :)



-- Foldr
-- max is reserved, we use maxKita untuk mengembalikan yang lebih besar dari 2 angka
maxKita :: Int -> Int -> Int
maxKita a b | a > b = a
            | otherwise = b

maxList xs = foldr (maxKita) 0 xs
-- maxList kita dengan base case 0, apabila array kosong akan return 0



-- Doing Concat to array of arrays
concatKita :: [[a]] -> [a]
concatKita [] = []
concatKita (xs:xss) = xs ++ (concat xss)
-- Can also done like this
-- concatKita xss = foldl (++) [] xss
