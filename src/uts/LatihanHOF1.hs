-- Latihan High Order Function nomor 1 (Exercises from chapter 9-10 of The Craft of Functional Programming)
import System.IO

-- 1. Define the length function using map and sum
lengthKita :: [Int] -> Int
lengthKita x = sum (map (\n -> 1) x)
-- test these lines after compiling
-- lengthKita [2,3,4]
-- lengthKita [1,2,3,4,6,10000]


-- 2. What does map (+1) (map (+1) xs) do?
mapKita :: [Int] -> [Int]
mapKita xs = map (+1) (map (+1) xs)
-- mapKita [1,2,3]


-- 3. Give the type of, and define the function iter so that iter n f x = f (f (... (f x)))
iter :: Int -> (a -> a) -> a -> a
iter 0 f a = a
iter n f a = iter (n-1) f (f a)


-- 4. How would you define the sum of the squares of the natural numbers 1 to n using map and foldr?
sumOfSquares :: Int -> Int
sumOfSquares n = foldr (+) 1 (map (^2) [2..n])
-- sumOfSquares 5


-- 5. How does the function
-- mystery xs = foldr (++) [] (map sing xs)
-- where sing x = [x]
-- behave?
-- It accepts array xs, and return the exact same array xs. sing x wraps x into an array, single-filled only with x.
-- map sing xs, wraps every item inside xs to a single-filled array, i.e. xs = [1,2,3,4] -> [[1],[2],[3],[4]]
-- foldr (++) [] folds right [] and merge every single one of (map sing xs), therefore merges everything back together


-- 6. If id is the polymorphic identity function, defined by id x = x, explain the behavior
-- of the expressions (id . f) (f . id) (id f)
-- (id . f) x = id (f x) therefore (f x)
-- (f . id) x = f (id x) therefore (f x)
-- (id f) x therefore (f x)


-- 7. Define a function composeList which composes a list of functions into a single function.
-- You should give the type of composeList
-- composeList :: Foldable t => t (a -> a) -> a -> a
-- composeList f fs = foldr (.) f fs
composeList :: Foldable t => t (a -> a) -> a -> a
composeList fs x = foldr (.) id fs x
-- composeList [(+1),(+2)] 2 should return 5


-- 8. (*) Define the function
-- flip :: (a -> b -> c) -> (b -> a -> c)
-- which reverses the order in which its function argument takes its arguments
myFlip f x y = f y x
newFunction = myFlip (\a -> \b -> a/b)
-- newFunction 10 20 -- would return 2
