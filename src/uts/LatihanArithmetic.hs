-- Understanding Expressions (Arithmetic Expressions)

-- data Expr = C Float | Add Expr Expr | Sub Expr Expr
--          | Mul Expr Expr | Div Expr Expr
data Expr = C Float | Expr :+ Expr | Expr :- Expr
         | Expr :* Expr | Expr :/ Expr
         | V [Char]
         | Let String Expr Expr
    deriving Show


subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)          = (C c)
subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :/ subst v0 e0 e2
-- Question from class, is this line below correct?
-- It was not, this is recursive call for multiple variables in the expression
-- Should be like this, if theres another variable in the expression,
-- substitute again like below
subst v0 e0 (Let v1 e1 e2) = Let v0 e0 (subst v1 e1 e2)

evaluate :: Expr -> Float
evaluate (C x) = x
evaluate (e1 :+ e2)    = evaluate e1 + evaluate e2
evaluate (e1 :- e2)    = evaluate e1 - evaluate e2
evaluate (e1 :* e2)    = evaluate e1 * evaluate e2
evaluate (e1 :/ e2)    = evaluate e1 / evaluate e2
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
-- Questions from class, is this line below correct?
-- Yes, if V is declared without telling any variables, set it to 0
evaluate (V v)         = 0.0

-- exp0 = (((C 5.0) :+ (C 7) :* (C 4)))
-- evaluate exp0
-- What does this line do? Takes constant 5, add it with a constant 7, times it with constant 4

-- Let's do expressions with variables now
-- exp1 = Let "x" (C 7) ((C 5) :+ (C 7) :* (V "x"))
-- evaluate exp1
-- Now what does this line do? Takes constant 5, add it with a constant 7, times it with variable x.
-- Where we declared: Let "x" is a constant 5
--
-- exp2 = (Let "x" (C 5) (Let "y" (V "x") (V "y" :* V "x")))
-- This should be correct now, 25.0


-- Defining our expressions like this, will ignore mathematical operations priority
-- 5 + 7 * 4 should've returned 33, but with this method returned 48 instead.
-- Because we defined the operations by our own, recursively, from left to right


-- Now let's practice with modifications
-- addWithExpressions :: Float -> Float -> Float
-- addWithExpressions x y = evaluate ((Let "x" (C x)) (Let "y" (C y)) (V "x") :+ (V "y"))
-- Just realized, based on line 19, definition cant be done like this

-- addWithExpressions x y = evaluate (evaluate (Let "x" (C x) (V "x")) :+ evaluate (Let "y" (C y) (V "y")))
-- Hmm this also cant be done, skipping due to time limitations, need to learn monad
