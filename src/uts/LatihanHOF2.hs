-- Latihan High Order Function nomor 2 (List Comprehensions and Higher-Order Functions)
import System.IO

-- 1. Can you rewrite the following list comprehensions using the higher-order functions map and filter?
-- You might need the function concat too
-- (a) [ x+1 | x <- xs ]
addOne :: [Int] -> [Int]
addOne xs = map (\n -> n+1) xs
-- addOne [1,2,3,4,5]

-- (b) [ x+y | x <- xs, y <-ys ]
sumAll :: [Int] -> [Int] -> [Int]
sumAll xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)
-- sumAll [1,2,3] [4,5,6]

-- (c) [ x+2 | x <- xs, x > 3 ]
addTwoIfBiggerThanThree :: [Int] -> [Int]
addTwoIfBiggerThanThree xs = map (\x -> x+2) (filter (>3) xs)
-- addTwoIfBiggerThanThree [2,3,4,5,6]

-- (d) [ x+3 | (x,_) <- xys ]
addLeftPairWithThree :: [(Int, Int)] -> [Int]
addLeftPairWithThree xys = map (\(x,_) -> x+3) xys
-- addLeftPairWithThree [(1,2),(2,2),(4,0)]

-- (e) [ x+4 | (x,y) <- xys, x+y < 5 ]
addLeftPairByFour :: [(Int, Int)] -> [Int]
addLeftPairByFour xys = map (\(x,_) -> x+4) (filter (\(x,y) -> x+y < 5) xys)
-- addLeftPairByFour [(1,2),(2,2),(4,0)]

-- (f) [ x+5 | Just x <- mxs ]
-- To be continued





-- 2. Can you do it the other way around? I.e. rewrite the following expressions as list comprehensions.
-- (a) map (+3) xs
mapPlusThree :: [Int] -> [Int]
mapPlusThree xs = [x+3 | x <- xs]
-- mapPlusThree [1,2,3]

-- (b) filter (>7) xs
filterMoreSeven :: [Int] -> [Int]
filterMoreSeven xs = [x | x <- xs, x > 7]
-- filterMoreSeven [2,4,8,12,30]

-- (c) concat (map (\x -> map (\y -> (x,y)) ys) xs)
concatLeftRight :: [Int] -> [Int] -> [(Int, Int)]
concatLeftRight xs ys = [(x,y) | x <- xs, y <- ys]
-- concatLeftRight [1,2] [3,4]

-- (d) filter (>3) (map (\(x,y) -> x+y) xys)
filterPairSumMoreThanThree :: [(Int, Int)] -> [Int]
filterPairSumMoreThanThree xys = [x+y | (x,y) <- xys, x+y > 3]
-- filterPairSumMoreThanThree [(1,2),(2,4),(4,4)]
