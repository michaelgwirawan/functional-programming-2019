-- Latihan ulang penjelasan yang diberikan di kelas sekitar HOF, Recursion, List Comprehension
import Data.List

-- Fibonacci
add (a:as) (b:bs) = (a + b):(add as bs)
fibs = 1 : 1 : add fibs (tail fibs)
-- fibs = 1,1,2,3,5,8,13..
-- fibs !! 5 -- would be 8


-- List Comprehensions
listFilter = [x+y | x <- [1..4], y <- [2..4], x > y]
-- listFilter would return [5,6,7]


divisor n = [x | x <- [1..n], n `mod` x == 0]
-- divisor 12 would return [1,2,3,4,6,12]


-- Lazy Evaluations
lebihKecilLazy n = [x | x <- [1..], x <= n]
divisorLazy n = [x | x <- (lebihKecilLazy n), n `mod` x == 0]
-- divisorLazy 12 would also return [1,2,3,4,6,12]


-- Quicksort
quicksort [] = []
quicksort (x:xs) = quicksort [y | y <- xs, y <= x] ++ [x] ++ quicksort [y | y <- xs, y > x]
-- quicksort [4,5,1,2,3,9,2,5]


-- Permutation
perm [] = [[]]
perm ls = [x:ps | x <- ls, ps <- perm (ls \\ [x])]
-- perm [1,2,3,4]


-- Pythagoras Triple
pythaTriple = [(x,y,z) | z <- [5..], y <- [z, z-1..1], x <- [y, y-1..1], x^2 + y^2 == z^2]
-- take 10 pythaTriple


-- Sieve of Erastothenes
primes = sieve [2..] where sieve (x:xs) = x : (sieve [z | z <- xs, z `mod` x /= 0])
-- take 10 primes


-- Hamming Code, starts with 1, merge 2x, 3x, 5x, no duplicates
merge xxs@(x:xs) yys@(y:ys) | x == y = x:merge xs ys
                            | x < y = x:merge xs yys
                            | x > y = y:merge xxs ys
hamming = 1 : merge (map (2*) hamming) (merge (map (3*) hamming) (map (5*) hamming))
-- take 20 hamming
