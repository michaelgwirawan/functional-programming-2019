import qualified Data.Char as Char
--- Nomer 1 Quiz Kelas B ---
getWord word [] = (word, [])
getWord word (x:xs) | (x == ' ') = (word, xs)
                    | (x /= ' ') = getWord (word ++ [x]) xs


splitter lw [] = lw
splitter lw input = let (word, rest) = getWord "" input
                    in splitter (lw ++ [word]) rest

upper el word = if word `elem` el
                then word
                else Char.toUpper (head word) : (tail word)

combine [] = []
combine [a] = a
combine (x:xs) = x ++ " " ++ (combine xs)

capitalize input = combine (map (upper ["is", "a"]) (splitter [] input))


-- Nomer 2 Quiz Kelas B ---
combineKita :: (b -> c) -> (a -> b) -> (a -> c)
combineKita f g x = f(g x)


--- Nomer 3 Quiz Kelas B ---
-- last = foldl (\x y -> y) 0
-- last [1,2,3,4,5,6]
-- this would return 6
