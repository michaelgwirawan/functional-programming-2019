--- Nomer 1 Quiz Kelas A ---
notNum x = x `notElem` "1234567890"
removeNum = filter notNum

doesNotHaveDuplicates (x:xs:xss) | (x==xs) = False
doesNotHaveDuplicates (x:xss) = doesNotHaveDuplicates xss
doesNotHaveDuplicates [] = True

isVowel x = x `elem` "aiueoAIUEO"
countVowel = length . filter isVowel
isThreeOrLess x = countVowel x < 4

weirdFilter = filter isThreeOrLess . filter doesNotHaveDuplicates . map removeNum


--- Nomer 2 Quiz Kelas A ---
rotabc :: String -> String
rotabc = map abc
  where abc 'a' = 'b'
        abc 'b' = 'c'
        abc 'c' = 'a'
        abc  x  =  x


--- Nomer 3 Quiz Kelas A ---
lastKita = head . reverse


--- Nomer 4 Quiz Kelas A ---
-- zcb t x k = when (at t) (scale x (one k))
