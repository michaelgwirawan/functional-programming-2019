// Nomer 8 Quiz Kelas B
const initialState = {toggled: false}
const reducer = (state, action) => {
  switch (action.type) {
    case "toggle":
      return {toggled: !state.toggled};
    default:
      throw new Error();
  }
}

const Toggle = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <ToDoItem selected={state.toggled} onClick={() => dispatch({type: "toggle"})} />
  )
}



// Nomer 8 Quiz Kelas B Remake multiple
const initialState = [{id: 1, name: "Initial Todo", completed: false}]
const reducer = (state, action) => {
  switch (action.type) {
    case "TOGGLE_COMPLETE":
      return state.map(item => item.id === action.id ? {...item, completed: !item.completed} : item);
  }
}

const Todos = () {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    {state.map(item => <ToDoItem key={todo.id} selected={item.completed} onClick={() => dispatch({type: "TOGGLE_COMPLETE", id: item.id})} />)}
  )
}
